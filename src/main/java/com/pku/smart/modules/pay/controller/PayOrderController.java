package com.pku.smart.modules.pay.controller;

import com.pku.smart.common.annotation.Log;
import com.pku.smart.common.base.BaseController;
import com.pku.smart.common.domain.AjaxResult;
import com.pku.smart.common.enums.BusinessType;
import com.pku.smart.common.page.TableDataInfo;
import com.pku.smart.modules.pay.entity.PayOrder;
import com.pku.smart.modules.pay.service.IPayOrderService;
import com.pku.smart.utils.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/pay/payorder")
public class PayOrderController extends BaseController {

    @Autowired
    IPayOrderService payOrderService;

    @PreAuthorize("@ss.hasPermi('pay:payorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(PayOrder order) {
        startPage();
        List<PayOrder> list = payOrderService.getOrderList(order);
        return getDataTable(list);
    }

    @Log(title = "支付订单", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('pay:payorder:export')")
    @GetMapping("/export")
    public AjaxResult export(PayOrder order) {
        List<PayOrder> list = payOrderService.getOrderList(order);
        ExcelUtil<PayOrder> util = new ExcelUtil<PayOrder>(PayOrder.class);
        return util.exportExcel(list, "支付订单");
    }

    @PreAuthorize("@ss.hasPermi('pay:payorder:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable String id) {
        return AjaxResult.success(payOrderService.selectPayOrder(id));
    }
}
